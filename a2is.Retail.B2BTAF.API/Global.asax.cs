﻿using a2is.Framework.API;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.SessionState;

namespace a2is.Retail.B2BTAF.API
{
    public class Global : System.Web.HttpApplication
    {
        protected void Application_Start(object sender, EventArgs e)
        {
            a2isAPIConfig.Configure();
        }
    }
}
