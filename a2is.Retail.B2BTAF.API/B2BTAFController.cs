﻿using a2is.Framework.API;
using a2is.Framework.DataAccess;
using a2is.Framework.Monitoring;
using a2is.retail.B2B.Entities.AAB_DB;
using a2is.retail.B2B.Entities.General;
using a2is.Retail.B2B.Entities.TAF;
using a2is.Retail.B2B.Helper.B2BAPI;
using a2is.Retail.B2B.Helper.Endorsement;
using a2is.Retail.B2BTAF.CancelOrder.BL.Manager;
using a2is.Retail.B2BTAF.NewOrder.BL.Manager;
using a2is.Retail.B2BTAF.NewOrder.BL.Model;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using RestSharp;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Globalization;
using System.Linq;
using System.Net;
using System.Reflection;
using System.Web;
using System.Web.Http;

namespace a2is.Retail.B2BTAF.API
{
    public class B2BTAFController : a2isAPIController
    {
        private static a2isLogHelper _log = new a2isLogHelper();

        [HttpPost, Route("NewOrder"), AllowAnonymous]
        public IHttpActionResult NewOrder(APIParam form)
         {
            ActionResult result = new ActionResult();

            try
            {
                _log.Debug("API New Order Start processing : " + form.RefferenceNo);

                #region request data from api
                string GetTaskDetailAPI = ConfigurationManager.AppSettings["GetTaskDetailAPI"];
                string B2BApplicationID_Order = ConfigurationManager.AppSettings["B2BApplicationID-Order"];
                #region Get data from B2B API
                /*
                object obj = new
                {
                    RefferenceNo = form.RefferenceNo,
                    B2BApplicationID = B2BApplicationID_Order
                };

                string json = JsonConvert.SerializeObject(obj);
                JObject jobject = JObject.Parse(json);

                var httpResponse = APIGetCaller(GetTaskDetailAPI, jobject, form.B2BToken);
                */
                #endregion
                #region GEt data Direct from maria DB
                ActionResult OrderB2BAPI = B2BAPI.GetB2BAPIData("2", B2BApplicationID_Order, form.RefferenceNo, "b2bsa");
                if (OrderB2BAPI.data == null)
                {
                    InsertLogNullData("1", form.RefferenceNo);
                    _log.Info("Get MariaDB is null " + form.RefferenceNo);
                    goto EndProcess;
                }
                #endregion
                #endregion

                NewOrderManager iManager = new NewOrderManager();
                _log.Debug("OrderB2BAPI.data : " + OrderB2BAPI.data.ToString());
                JObject DataObject = JObject.Parse(OrderB2BAPI.data);
                string DictRequestData = Convert.ToString(DataObject["RequestData"]);

                if (DictRequestData != null)
                {
                    string param = "NewOrder";

                    result = iManager.ParseRequestDataNewOrder(DictRequestData);

                    if (result.isSuccess)
                    {
                        result.data = iManager.ReceiveNewOrder(result.data);
                        #region cek error
                        if(result.data.TrxNewOrderTmp.Error_Code1 != 0 || result.data.TrxNewOrderTmp.Error_Code2 != 0 || result.data.TrxNewOrderTmp.Error_Description.Trim() != "Success")
                        {
                            result.isSuccess = false;
                            result.message = result.data.TrxNewOrderTmp.Error_Description;
                        }
                        #endregion
                    }
                    else
                    {
                        result.data.TrxNewOrderTmp.Error_Description = "Error Parsing : " + result.message;
                    }
                    result = InsertLog(result.data, param, form.RefferenceNo, result);
                }              
            }
            catch (Exception ex)
            {
                result.isSuccess = false;
                result.message = ex.Message;
                _log.Error(MethodBase.GetCurrentMethod().Name + "Error : " + result.message);
                InsertLogErrorData(result.data, "NewOrder", form.RefferenceNo, ex.Message);
            }

            EndProcess:
            _log.Debug("API New Order End");
            return Json(result);
        }

        [HttpPost, Route("CancelOrder"), AllowAnonymous]
        public IHttpActionResult CancelOrder(APIParam form)
        {
            ActionResult result = new ActionResult();
            try
            {
                _log.Debug("API Cancellation Start");

                #region request data 
                string GetTaskDetailAPI = ConfigurationManager.AppSettings["GetTaskDetailAPI"];
                string B2BApplicationID = ConfigurationManager.AppSettings["B2BApplicationID-Cancel"];

                object obj = new
                {
                    RefferenceNo = form.RefferenceNo,
                    B2BApplicationID = ConfigurationManager.AppSettings["B2BApplicationID-Cancel"]
                };

                string json = JsonConvert.SerializeObject(obj);
                JObject jobject = JObject.Parse(json);
                #region Get data from B2B API
                //var httpResponse = APIGetCaller(GetTaskDetailAPI, jobject, form.B2BToken);
                #endregion
                #region GEt data Direct from maria DB
                ActionResult OrderB2BAPI = B2BAPI.GetB2BAPIData("2", B2BApplicationID, form.RefferenceNo, "b2bsa");
                if (OrderB2BAPI.data == null)
                {
                    InsertLogNullData("1", form.RefferenceNo);
                    _log.Info("Get MariaDB is null " + form.RefferenceNo);
                    goto EndProcess;
                }
                #endregion
                #endregion

                //if (httpResponse.StatusCode == HttpStatusCode.OK)
                //{
                //JObject DataObject = JObject.Parse(httpResponse.Content);
                //Dictionary<string, string> DictData = JsonConvert.DeserializeObject<Dictionary<string, string>>(DataObject["data"].ToString());
                //if (DictData != null && DictData.Count > 0)
                //{
                string DictRequestData = Convert.ToString(OrderB2BAPI.data.ToString());
                //string DictB2BRequestID = Convert.ToString(DictData["b2BRequestID"]);
                if (DictRequestData.Length > 0)
                {
                    result = ParseRequestDataCancellation(DictRequestData);

                    if (result.isSuccess)
                    {
                        result.data.Order_Type = "4";
                        CancellationManager iManager = new CancellationManager();
                        result = iManager.ProcessCancellation(result.data);

                        if (result.isSuccess == false)
                        {
                            if(result.data.Error_Code1 != 0)
                            {
                                result.data.Error_Code1 = 9999;
                                result.data.Error_Description = result.message;
                            }
                        }
                    }
                    else
                    {
                        result.data.Error_Description = "Error Parsing : " + result.message;
                    }

                    result = InsertLog(result.data, "CancelOrder", form.RefferenceNo);
                }
            }
            catch (Exception ex)
            {
                result.isSuccess = false;
                result.message = ex.Message;
                _log.Error(MethodBase.GetCurrentMethod().Name + "Error : " + result.message);
            }
            EndProcess:
            _log.Debug("API Cancellation End");
            return Json(result);
        }

        private IRestResponse APIGetCaller(string urlhost, JObject parameter, string token = null)
        {
            ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls12 | SecurityProtocolType.Tls11 | SecurityProtocolType.Tls;

            var client = new RestClient(urlhost);

            var request = new RestRequest(Method.POST);

            ServicePointManager.ServerCertificateValidationCallback += (sender, cert, chain, sslPolicyErrors) => true;

            request.AddHeader("accept", "application/json");

            request.AddHeader("content-type", "application/json");

            request.AddHeader("Authorization", "Bearer " + token);

            request.AddParameter("application/json", parameter, ParameterType.RequestBody);

            return client.Execute(request);
        }
        private void UpdateB2BRequestStatus(string B2BToken, string reffNo, int enumRequestStatus, string B2BApplicationID, string B2BRequestID)
        {
            B2BAPI.UpdateRequestStatus(B2BApplicationID, reffNo, B2BRequestID.ToString(), enumRequestStatus.ToString(), B2BToken);
        }
        /*
        public ActionResult ParseRequestDataNewOrder(string jsonRequestData)
        {
            ActionResult result = new ActionResult();
            TAFNewOrder oTAFNewOrder = new TAFNewOrder();

            var requestDataParse = JsonConvert.DeserializeObject<dynamic>(jsonRequestData);
            string requestData = requestDataParse.RequestData.ToString();
            var settings = new JsonSerializerSettings
            {
                NullValueHandling = NullValueHandling.Ignore,
                MissingMemberHandling = MissingMemberHandling.Ignore
            };
            var result0 = JsonConvert.DeserializeObject<dynamic>(requestData, settings);
            string jsonAABTrxNewOrderTmp = result0.AABTrxNewOrderTmp.ToString();
            string jsonAABOrderInterestTmp = result0.AABOrderInterestTmp.ToString();

            oTAFNewOrder.TrxNewOrderTmp = JsonConvert.DeserializeObject<AABTrxNewOrderTmp>(jsonAABTrxNewOrderTmp, settings);
            oTAFNewOrder.OrderInterestTMP = JsonConvert.DeserializeObject<AABOrderInterestTMP>(jsonAABOrderInterestTmp, settings);
            result.data = oTAFNewOrder;
            result.isSuccess = true;

            return result;
        }
        */
        public ActionResult ParseRequestDataCancellation(string jsonRequestData)
        {
            ActionResult result = new ActionResult();
            TAFCancelOrder oTAFCancelOrder = new TAFCancelOrder();
            string propName = "";

            var requestDataParse = JsonConvert.DeserializeObject<dynamic>(jsonRequestData);
            string requestData = requestDataParse["RequestData"].ToString();
            var settings = new JsonSerializerSettings
            {
                NullValueHandling = NullValueHandling.Ignore,
                MissingMemberHandling = MissingMemberHandling.Ignore
            };
            var result0 = JsonConvert.DeserializeObject<dynamic>(requestData, settings);


            var properties = oTAFCancelOrder.GetType().GetProperties();
            foreach (PropertyInfo prop in properties)
            {
                string sVal = "";
                propName = prop.Name;
                if (result0[prop.Name] != null)
                {
                    sVal = result0[prop.Name].ToString();
                }

                var type = Nullable.GetUnderlyingType(prop.PropertyType) ?? prop.PropertyType;

                if (type == typeof(DateTime))
                {
                    DateTime newDate = new DateTime(1900, 1, 1);

                    if (sVal.Trim() == "" || sVal.Trim() == "null")
                    {
                        prop.SetValue(oTAFCancelOrder, newDate, null);
                    }
                    else if (result0[prop.Name].Value.GetType().Name == "DateTime")
                    {
                        newDate = Convert.ToDateTime(result0[prop.Name].Value);
                        prop.SetValue(oTAFCancelOrder, newDate, null);
                    }
                    else if (DateTime.TryParseExact(sVal, "dd/MM/yyyy", CultureInfo.InvariantCulture, DateTimeStyles.None, out newDate))
                    {
                        prop.SetValue(oTAFCancelOrder, newDate, null);
                    }
                    else
                    {
                        result.isSuccess = false;
                        result.message = "Error while parsing data tipe datetime : " + sVal;
                        //_log.Error(MethodBase.GetCurrentMethod().Name + "Error while parsing datetime : " + sVal);

                        return result;
                    }
                }
                else if (type == typeof(decimal))
                {
                    if (sVal == "")
                        sVal = "0";
                    decimal decimalVal = Convert.ToDecimal(sVal);
                    prop.SetValue(oTAFCancelOrder, decimalVal, null);
                }
                else if (type == typeof(double))
                {
                    if (sVal == "")
                        sVal = "0";
                    double doubleVal = Convert.ToDouble(sVal);
                    prop.SetValue(oTAFCancelOrder, doubleVal, null);
                }
                else if (type == typeof(int))
                {
                    if (sVal == "")
                        sVal = "0";
                    int intVal = Convert.ToInt32(sVal);
                    prop.SetValue(oTAFCancelOrder, intVal, null);
                }
                else //string
                {
                    prop.SetValue(oTAFCancelOrder, sVal, null);
                }

            }




            result.data = oTAFCancelOrder;
            result.isSuccess = true;

            return result;

        }
        #region Insert Logs
        public ActionResult InsertLog(dynamic data, string param, string reffNo, ActionResult result = null)
        {
            string UserID = ConfigurationManager.AppSettings["EntryUserID"];

            try
            {
                using (a2isDBHelper.Database db = new a2isDBHelper.Database("a2isRetailDB"))
                {
                    #region query insert partner order log
                    string qInsertPartnerOrderLog = @";
                                        DECLARE @@Contract_Number VARCHAR(50) = @0
                                                ,@@Product_Code VARCHAR(50) = @1
                                                ,@@Partner_Branch_Code VARCHAR(50) = @2
                                                ,@@Engine_Number VARCHAR(50) = @3
                                                ,@@Chassis_Number VARCHAR(50) = @4
                                                ,@@Period_From VARCHAR(50) = @5
                                                ,@@Period_To VARCHAR(50) = @6
                                                ,@@Client_Number VARCHAR(50) = @7
                                                ,@@Vehicle_Code VARCHAR(50) = @8
                                                ,@@Geographical_Area VARCHAR(50) = @9
                                                ,@@Manufacturing_Year VARCHAR(50) = @10
                                                ,@@Order_Type VARCHAR(50) = @11
                                                ,@@Policy_No VARCHAR(50) = @12
                                                ,@@Endorsement_No VARCHAR(50) = @13
                                                ,@@Effective_Date VARCHAR(50) = @14
                                                ,@@Name VARCHAR(50) = @15
                                                ,@@Error_Code VARCHAR(255) = @16
                                                ,@@Error_Description VARCHAR(255) = @17
                                                ,@@entryusr VARCHAR(50) = @18
                                                ,@@entrydt VARCHAR(50) = @19
                                                ,@@updateusr VARCHAR(50) = @20
                                                ,@@updatedt VARCHAR(50) = @21
                                                ,@@Partner_Id VARCHAR(50) = @22
                                                ,@@Numeric_Error_Code_1 VARCHAR(50) = @23
                                                ,@@Numeric_Error_Code_2 VARCHAR(50) = @24
                                                ,@@Object_No VARCHAR(50) = @25
                                                ,@@Endorsement_Map VARCHAR(50) = @26
                                                ,@@guid VARCHAR(50) = @27
                                                ,@@Error_Description2 VARCHAR(255) = @28
                                                ,@@Flag_Value_Chain VARCHAR(50) = @29
                                                ,@@GROUP_NO VARCHAR(50) = @30
                                                ,@@Send_Status INT = @31
                                        INSERT INTO [dbo].[Partner_Orders_Log]
                                                   ([Contract_Number]
                                                   ,[Product_Code]
                                                   ,[Partner_Branch_Code]
                                                   ,[Engine_Number]
                                                   ,[Chassis_Number]
                                                   ,[Period_From]
                                                   ,[Period_To]
                                                   ,[Client_Number]
                                                   ,[Vehicle_Code]
                                                   ,[Geographical_Area]
                                                   ,[Manufacturing_Year]
                                                   ,[Order_Type]
                                                   ,[Policy_No]
                                                   ,[Endorsement_No]
                                                   ,[Effective_Date]
                                                   ,[Name]
                                                   ,[Error_Code]
                                                   ,[Error_Description]
                                                   ,[entryusr]
                                                   ,[entrydt]
                                                   ,[updateusr]
                                                   ,[updatedt]
                                                   ,[Partner_Id]
                                                   ,[Numeric_Error_Code_1]
                                                   ,[Numeric_Error_Code_2]
                                                   ,[Object_No]
                                                   ,[Endorsement_Map]
                                                   ,[guid]
                                                   ,[Error_Description2]
                                                   ,[Flag_Value_Chain]
                                                   ,[GROUP_NO]
                                                   ,[Send_Status])
                                             VALUES
                                                   (@@Contract_Number
                                                   ,@@Product_Code
                                                   ,@@Partner_Branch_Code
                                                   ,@@Engine_Number
                                                   ,@@Chassis_Number
                                                   ,@@Period_From
                                                   ,@@Period_To
                                                   ,@@Client_Number
                                                   ,@@Vehicle_Code
                                                   ,@@Geographical_Area
                                                   ,@@Manufacturing_Year
                                                   ,@@Order_Type
                                                   ,@@Policy_No
                                                   ,@@Endorsement_No
                                                   ,@@Effective_Date
                                                   ,@@Name
                                                   ,@@Error_Code
                                                   ,@@Error_Description
                                                   ,@@entryusr
                                                   ,@@entrydt
                                                   ,@@updateusr
                                                   ,@@updatedt
                                                   ,@@Partner_Id
                                                   ,@@Numeric_Error_Code_1
                                                   ,@@Numeric_Error_Code_2
                                                   ,@@Object_No
                                                   ,@@Endorsement_Map
                                                   ,@@guid
                                                   ,@@Error_Description2
                                                   ,@@Flag_Value_Chain
                                                   ,@@GROUP_NO
                                                   ,@@Send_Status)
                                        ";
                    #endregion
                    if (param == "NewOrder")
                    {
                        #region insert log into partner_orders_log
                        db.Execute(qInsertPartnerOrderLog,
                                data.TrxNewOrderTmp.Contract_Number //@@Contract_Number
                                , data.TrxNewOrderTmp.Product_Code //@@Product_Code
                                , data.TrxNewOrderTmp.Partner_Branch_Code //@@Partner_Branch_Code
                                , data.OrderInterestTMP.Engine_Number //@@Engine_Number
                                , data.OrderInterestTMP.Chasis_Number //@@Chassis_Number
                                , data.TrxNewOrderTmp.Period_From.ToString("MM/dd/yyyy") //@@Period_From
                                , data.TrxNewOrderTmp.Period_To.ToString("MM/dd/yyyy") //@@Period_To
                                , data.TrxNewOrderTmp.Client_Number //@@Client_Number
                                , data.OrderInterestTMP.Vehicle_Code //@@Vehicle_Code
                                , data.TrxNewOrderTmp.Geographical_Area //@@Geographical_Area
                                , data.OrderInterestTMP.Manufacturing_Year //@@Manufacturing_Year
                                , "1" //@@Order_Type
                                , null//@@Policy_No
                                , null//@@Endorsement_No
                                , data.TrxNewOrderTmp.Period_From.ToString("yyyy/MM/dd HH:mm:ss") //@@Effective_Date
                                , data.TrxNewOrderTmp.Name //@Name
                                , null //@@Error_Code
                                , data.TrxNewOrderTmp.Error_Description //@@Error_Description
                                , UserID //@@entryusr
                                , DateTime.Now //@@entrydt
                                , UserID //@@updateusr
                                , DateTime.Now //@@updatedt
                                , "TAF" //@@Partner_Id
                                , data.TrxNewOrderTmp.Error_Code1 //@@Numeric_Error_Code_1
                                , data.TrxNewOrderTmp.Error_Code2 //@@Numeric_Error_Code_2
                                , data.OrderInterestTMP.Object_Number //@@Object_No
                                , null //@@Endorsement_Map
                                , null //@@guid
                                , null //@@Error_Description2
                                , null //@@Flag_Value_Chain
                                , null //@@GROUP_NO
                                , 0);

                        #endregion
                    }
                    else if (param == "CancelOrder")
                    {
                        #region insert into partner_order_log
                        int res = db.Execute(qInsertPartnerOrderLog,
                                data.ContractNumber == null ? "" : data.ContractNumber //@@Contract_Number
                                , data.ProductCode //@@Product_Code
                                , null //@@Partner_Branch_Code
                                , null //@@Engine_Number
                                , null //@@Chassis_Number
                                , null //@@Period_From
                                , null //@@Period_To
                                , null //@@Client_Number
                                , null //@@Vehicle_Code
                                , null //@@Geographical_Area
                                , null //@@Manufacturing_Year
                                , data.Order_Type //@@Order_Type
                                , data.PolicyNumber //@@Policy_No
                                , EndorsementDataAccess.GetLastEndorsementNo(data.PolicyNumber) //@@Endorsement_No
                                , data.Effective_Date == DateTime.MinValue ? null : data.Effective_Date.ToString("MM/dd/yyyy") //@@Effective_Date
                                , null //@@Name
                                , null //@@Error_Code
                                , data.Error_Description //@@Error_Description
                                , UserID //@@entryusr
                                , DateTime.Now //@@entrydt
                                , null //@@updateusr
                                , null //@@updatedt
                                , "TAF" //@@Partner_Id
                                , data.Error_Code1 //@@Numeric_Error_Code_1
                                , data.Error_Code2 //@@Numeric_Error_Code_2
                                , null //@@Object_No
                                , null //@@Endorsement_Map
                                , null //@@guid
                                , null //@@Error_Description2
                                , null //@@Flag_Value_Chain
                                , null //@@GROUP_NO
                                , 0
                                );
                        result.message = data.Error_Description;
                        #endregion
                    }
                    else if (param == "groupingAR")
                    {
                        #region insert log into acc_grouping_ar
                        #region query
                        string query = @"
                                DECLARE @@no_kontrak VARCHAR(50) = @0
                                        ,@@policy_no VARCHAR(50) = @1
                                        ,@@group_no VARCHAR(50) = @2
                                        ,@@approval_date VARCHAR(50) = @3
                                        ,@@type_insu VARCHAR(50) = @4
                                        ,@@endorsement_no VARCHAR(50) = @5
                                        ,@@ar_status VARCHAR(50) = @6
                                        ,@@send_status VARCHAR(50) = @7
                                        ,@@send_date VARCHAR(50) = @8
                                        ,@@error_code VARCHAR(50) = @9
                                        ,@@error_description VARCHAR(250) = @10
                                        ,@@reff_no VARCHAR(50) = @11
                                        ,@@file_name VARCHAR(50) = @12
                                        ,@@create_by VARCHAR(50) = @13
                                        ,@@create_date DATETIME = @14

                                INSERT INTO [dbo].[acc_grouping_ar]
                                           ([no_kontrak]
                                           ,[policy_no]
                                           ,[group_no]
                                           ,[approval_date]
                                           ,[type_insu]
                                           ,[endorsement_no]
                                           ,[ar_status]
                                           ,[send_status]
                                           ,[send_date]
                                           ,[error_code]
                                           ,[error_description]
                                           ,[reff_no]
                                           ,[file_name]
                                           ,[create_by]
                                           ,[create_date])
                                     VALUES
                                           (@@no_kontrak
                                           ,@@policy_no
                                           ,@@group_no
                                           ,@@approval_date
                                           ,@@type_insu
                                           ,@@endorsement_no
                                           ,@@ar_status
                                           ,@@send_status
                                           ,@@send_date
                                           ,@@error_code
                                           ,@@error_description
                                           ,@@reff_no
                                           ,@@file_name
                                           ,@@create_by
                                           ,@@create_date)";
                        #endregion

                        foreach (var item in data)
                        {
                            db.Execute(query, item.NO_KONTRAK, item.POLICY_NO, item.GROUP_NO, item.APPROVAL_DATE.ToString("dd/MM/yyyy"), item.TYPE_INSU, "0", "1", "0", "", item.ERROR_CODE, item.ERROR_DESCRIPTION, reffNo, "",
                                UserID, DateTime.Now);
                        }
                        #endregion
                    }
                    else if (param == "nullData")
                    {
                        #region insert into partner_order_log
                        int res = db.Execute(qInsertPartnerOrderLog,
                                data.ContractNumber == null ? "" : data.ContractNumber //@@Contract_Number
                                , data.ProductCode //@@Product_Code
                                , null //@@Partner_Branch_Code
                                , null //@@Engine_Number
                                , null //@@Chassis_Number
                                , null //@@Period_From
                                , null //@@Period_To
                                , null //@@Client_Number
                                , null //@@Vehicle_Code
                                , null //@@Geographical_Area
                                , null //@@Manufacturing_Year
                                , data.Order_Type //@@Order_Type
                                , data.PolicyNumber //@@Policy_No
                                , null//@@Endorsement_No
                                , data.Effective_Date == DateTime.MinValue ? null : data.Effective_Date.ToString("MM/dd/yyyy") //@@Effective_Date
                                , null //@@Name
                                , null //@@Error_Code
                                , "error get data from Maria DB" //@@Error_Description
                                , UserID //@@entryusr
                                , DateTime.Now //@@entrydt
                                , null //@@updateusr
                                , null //@@updatedt
                                , "TAF" //@@Partner_Id
                                , data.Error_Code1 //@@Numeric_Error_Code_1
                                , data.Error_Code2 //@@Numeric_Error_Code_2
                                , null //@@Object_No
                                , null //@@Endorsement_Map
                                , null //@@guid
                                , null //@@Error_Description2
                                , null //@@Flag_Value_Chain
                                , null //@@GROUP_NO
                                , 0
                                );
                        #endregion
                    }
                }
            }
            catch (Exception ex)
            {
                result.isSuccess = false;
                result.message = ex.Message;
                _log.Error(MethodBase.GetCurrentMethod().Name + "Error : " + ex.Message);
            }

            return result;
        }
        public ActionResult InsertLogNullData(string orderType, string reffNo)
        {
            ActionResult result = new ActionResult();

            string UserID = ConfigurationManager.AppSettings["EntryUserID"];

            try
            {
                using (a2isDBHelper.Database db = new a2isDBHelper.Database("a2isRetailDB"))
                {
                    #region query insert partner order log
                    string qInsertPartnerOrderLog = @";
                                        DECLARE @@Contract_Number VARCHAR(50) = @0
                                                ,@@Product_Code VARCHAR(50) = @1
                                                ,@@Partner_Branch_Code VARCHAR(50) = @2
                                                ,@@Engine_Number VARCHAR(50) = @3
                                                ,@@Chassis_Number VARCHAR(50) = @4
                                                ,@@Period_From VARCHAR(50) = @5
                                                ,@@Period_To VARCHAR(50) = @6
                                                ,@@Client_Number VARCHAR(50) = @7
                                                ,@@Vehicle_Code VARCHAR(50) = @8
                                                ,@@Geographical_Area VARCHAR(50) = @9
                                                ,@@Manufacturing_Year VARCHAR(50) = @10
                                                ,@@Order_Type VARCHAR(50) = @11
                                                ,@@Policy_No VARCHAR(50) = @12
                                                ,@@Endorsement_No VARCHAR(50) = @13
                                                ,@@Effective_Date VARCHAR(50) = @14
                                                ,@@Name VARCHAR(50) = @15
                                                ,@@Error_Code VARCHAR(255) = @16
                                                ,@@Error_Description VARCHAR(255) = @17
                                                ,@@entryusr VARCHAR(50) = @18
                                                ,@@entrydt VARCHAR(50) = @19
                                                ,@@updateusr VARCHAR(50) = @20
                                                ,@@updatedt VARCHAR(50) = @21
                                                ,@@Partner_Id VARCHAR(50) = @22
                                                ,@@Numeric_Error_Code_1 VARCHAR(50) = @23
                                                ,@@Numeric_Error_Code_2 VARCHAR(50) = @24
                                                ,@@Object_No VARCHAR(50) = @25
                                                ,@@Endorsement_Map VARCHAR(50) = @26
                                                ,@@guid VARCHAR(50) = @27
                                                ,@@Error_Description2 VARCHAR(255) = @28
                                                ,@@Flag_Value_Chain VARCHAR(50) = @29
                                                ,@@GROUP_NO VARCHAR(50) = @30
                                                ,@@Send_Status INT = @31
                                        INSERT INTO [dbo].[Partner_Orders_Log]
                                                   ([Contract_Number]
                                                   ,[Product_Code]
                                                   ,[Partner_Branch_Code]
                                                   ,[Engine_Number]
                                                   ,[Chassis_Number]
                                                   ,[Period_From]
                                                   ,[Period_To]
                                                   ,[Client_Number]
                                                   ,[Vehicle_Code]
                                                   ,[Geographical_Area]
                                                   ,[Manufacturing_Year]
                                                   ,[Order_Type]
                                                   ,[Policy_No]
                                                   ,[Endorsement_No]
                                                   ,[Effective_Date]
                                                   ,[Name]
                                                   ,[Error_Code]
                                                   ,[Error_Description]
                                                   ,[entryusr]
                                                   ,[entrydt]
                                                   ,[updateusr]
                                                   ,[updatedt]
                                                   ,[Partner_Id]
                                                   ,[Numeric_Error_Code_1]
                                                   ,[Numeric_Error_Code_2]
                                                   ,[Object_No]
                                                   ,[Endorsement_Map]
                                                   ,[guid]
                                                   ,[Error_Description2]
                                                   ,[Flag_Value_Chain]
                                                   ,[GROUP_NO]
                                                   ,[Send_Status])
                                             VALUES
                                                   (@@Contract_Number
                                                   ,@@Product_Code
                                                   ,@@Partner_Branch_Code
                                                   ,@@Engine_Number
                                                   ,@@Chassis_Number
                                                   ,@@Period_From
                                                   ,@@Period_To
                                                   ,@@Client_Number
                                                   ,@@Vehicle_Code
                                                   ,@@Geographical_Area
                                                   ,@@Manufacturing_Year
                                                   ,@@Order_Type
                                                   ,@@Policy_No
                                                   ,@@Endorsement_No
                                                   ,@@Effective_Date
                                                   ,@@Name
                                                   ,@@Error_Code
                                                   ,@@Error_Description
                                                   ,@@entryusr
                                                   ,@@entrydt
                                                   ,@@updateusr
                                                   ,@@updatedt
                                                   ,@@Partner_Id
                                                   ,@@Numeric_Error_Code_1
                                                   ,@@Numeric_Error_Code_2
                                                   ,@@Object_No
                                                   ,@@Endorsement_Map
                                                   ,@@guid
                                                   ,@@Error_Description2
                                                   ,@@Flag_Value_Chain
                                                   ,@@GROUP_NO
                                                   ,@@Send_Status)
                                        ";
                    #endregion

                    #region insert into partner_order_log
                    int res = db.Execute(qInsertPartnerOrderLog,
                            reffNo //@@Contract_Number
                            , null //@@Product_Code
                            , null //@@Partner_Branch_Code
                            , null //@@Engine_Number
                            , null //@@Chassis_Number
                            , null //@@Period_From
                            , null //@@Period_To
                            , null //@@Client_Number
                            , null //@@Vehicle_Code
                            , null //@@Geographical_Area
                            , null //@@Manufacturing_Year
                            , orderType //@@Order_Type
                            , null //@@Policy_No
                            , null//@@Endorsement_No
                            , null //@@Effective_Date
                            , null //@@Name
                            , null //@@Error_Code
                            , "error get data from Maria DB" //@@Error_Description
                            , UserID //@@entryusr
                            , DateTime.Now //@@entrydt
                            , null //@@updateusr
                            , null //@@updatedt
                            , "TAF" //@@Partner_Id
                            , null //@@Numeric_Error_Code_1
                            , null //@@Numeric_Error_Code_2
                            , null //@@Object_No
                            , null //@@Endorsement_Map
                            , null //@@guid
                            , null //@@Error_Description2
                            , null //@@Flag_Value_Chain
                            , null //@@GROUP_NO
                            , 0
                            );
                    #endregion

                }
            }
            catch (Exception ex)
            {
                result.isSuccess = false;
                result.message = ex.Message;
                _log.Error(MethodBase.GetCurrentMethod().Name + "Error : " + ex.Message);
            }

            return result;
        }
        public ActionResult InsertLogErrorData(dynamic data, string param, string reffNo, string errorDesc)
        {
            ActionResult result = new ActionResult();

            string UserID = ConfigurationManager.AppSettings["EntryUserID"];

            try
            {
                using (a2isDBHelper.Database db = new a2isDBHelper.Database("a2isRetailDB"))
                {
                    #region query insert partner order log
                    string qInsertPartnerOrderLog = @";
                                        DECLARE @@Contract_Number VARCHAR(50) = @0
                                                ,@@Product_Code VARCHAR(50) = @1
                                                ,@@Partner_Branch_Code VARCHAR(50) = @2
                                                ,@@Engine_Number VARCHAR(50) = @3
                                                ,@@Chassis_Number VARCHAR(50) = @4
                                                ,@@Period_From VARCHAR(50) = @5
                                                ,@@Period_To VARCHAR(50) = @6
                                                ,@@Client_Number VARCHAR(50) = @7
                                                ,@@Vehicle_Code VARCHAR(50) = @8
                                                ,@@Geographical_Area VARCHAR(50) = @9
                                                ,@@Manufacturing_Year VARCHAR(50) = @10
                                                ,@@Order_Type VARCHAR(50) = @11
                                                ,@@Policy_No VARCHAR(50) = @12
                                                ,@@Endorsement_No VARCHAR(50) = @13
                                                ,@@Effective_Date VARCHAR(50) = @14
                                                ,@@Name VARCHAR(50) = @15
                                                ,@@Error_Code VARCHAR(255) = @16
                                                ,@@Error_Description VARCHAR(255) = @17
                                                ,@@entryusr VARCHAR(50) = @18
                                                ,@@entrydt VARCHAR(50) = @19
                                                ,@@updateusr VARCHAR(50) = @20
                                                ,@@updatedt VARCHAR(50) = @21
                                                ,@@Partner_Id VARCHAR(50) = @22
                                                ,@@Numeric_Error_Code_1 VARCHAR(50) = @23
                                                ,@@Numeric_Error_Code_2 VARCHAR(50) = @24
                                                ,@@Object_No VARCHAR(50) = @25
                                                ,@@Endorsement_Map VARCHAR(50) = @26
                                                ,@@guid VARCHAR(50) = @27
                                                ,@@Error_Description2 VARCHAR(255) = @28
                                                ,@@Flag_Value_Chain VARCHAR(50) = @29
                                                ,@@GROUP_NO VARCHAR(50) = @30
                                                ,@@Send_Status INT = @31
                                        INSERT INTO [dbo].[Partner_Orders_Log]
                                                   ([Contract_Number]
                                                   ,[Product_Code]
                                                   ,[Partner_Branch_Code]
                                                   ,[Engine_Number]
                                                   ,[Chassis_Number]
                                                   ,[Period_From]
                                                   ,[Period_To]
                                                   ,[Client_Number]
                                                   ,[Vehicle_Code]
                                                   ,[Geographical_Area]
                                                   ,[Manufacturing_Year]
                                                   ,[Order_Type]
                                                   ,[Policy_No]
                                                   ,[Endorsement_No]
                                                   ,[Effective_Date]
                                                   ,[Name]
                                                   ,[Error_Code]
                                                   ,[Error_Description]
                                                   ,[entryusr]
                                                   ,[entrydt]
                                                   ,[updateusr]
                                                   ,[updatedt]
                                                   ,[Partner_Id]
                                                   ,[Numeric_Error_Code_1]
                                                   ,[Numeric_Error_Code_2]
                                                   ,[Object_No]
                                                   ,[Endorsement_Map]
                                                   ,[guid]
                                                   ,[Error_Description2]
                                                   ,[Flag_Value_Chain]
                                                   ,[GROUP_NO]
                                                   ,[Send_Status])
                                             VALUES
                                                   (@@Contract_Number
                                                   ,@@Product_Code
                                                   ,@@Partner_Branch_Code
                                                   ,@@Engine_Number
                                                   ,@@Chassis_Number
                                                   ,@@Period_From
                                                   ,@@Period_To
                                                   ,@@Client_Number
                                                   ,@@Vehicle_Code
                                                   ,@@Geographical_Area
                                                   ,@@Manufacturing_Year
                                                   ,@@Order_Type
                                                   ,@@Policy_No
                                                   ,@@Endorsement_No
                                                   ,@@Effective_Date
                                                   ,@@Name
                                                   ,@@Error_Code
                                                   ,@@Error_Description
                                                   ,@@entryusr
                                                   ,@@entrydt
                                                   ,@@updateusr
                                                   ,@@updatedt
                                                   ,@@Partner_Id
                                                   ,@@Numeric_Error_Code_1
                                                   ,@@Numeric_Error_Code_2
                                                   ,@@Object_No
                                                   ,@@Endorsement_Map
                                                   ,@@guid
                                                   ,@@Error_Description2
                                                   ,@@Flag_Value_Chain
                                                   ,@@GROUP_NO
                                                   ,@@Send_Status)
                                        ";
                    #endregion
                    if (param == "NewOrder")
                    {
                        if (data != null)
                        {
                            #region insert log into partner_orders_log
                            db.Execute(qInsertPartnerOrderLog,
                                    data.TrxNewOrderTmp.Contract_Number //@@Contract_Number
                                    , data.TrxNewOrderTmp.Product_Code //@@Product_Code
                                    , data.TrxNewOrderTmp.Partner_Branch_Code //@@Partner_Branch_Code
                                    , data.OrderInterestTMP.Engine_Number //@@Engine_Number
                                    , data.OrderInterestTMP.Chasis_Number //@@Chassis_Number
                                    , data.TrxNewOrderTmp.Period_From.ToString("MM/dd/yyyy") //@@Period_From
                                    , data.TrxNewOrderTmp.Period_To.ToString("MM/dd/yyyy") //@@Period_To
                                    , data.TrxNewOrderTmp.Client_Number //@@Client_Number
                                    , data.OrderInterestTMP.Vehicle_Code //@@Vehicle_Code
                                    , data.TrxNewOrderTmp.Geographical_Area //@@Geographical_Area
                                    , data.OrderInterestTMP.Manufacturing_Year //@@Manufacturing_Year
                                    , "1" //@@Order_Type
                                    , null//@@Policy_No
                                    , null//@@Endorsement_No
                                    , data.TrxNewOrderTmp.Period_From.ToString("yyyy/MM/dd HH:mm:ss") //@@Effective_Date
                                    , data.TrxNewOrderTmp.Name //@Name
                                    , null //@@Error_Code
                                    , errorDesc //@@Error_Description
                                    , UserID //@@entryusr
                                    , DateTime.Now //@@entrydt
                                    , UserID //@@updateusr
                                    , DateTime.Now //@@updatedt
                                    , "TAF" //@@Partner_Id
                                    , data.TrxNewOrderTmp.Error_Code1 //@@Numeric_Error_Code_1
                                    , data.TrxNewOrderTmp.Error_Code2 //@@Numeric_Error_Code_2
                                    , data.OrderInterestTMP.Object_Number //@@Object_No
                                    , null //@@Endorsement_Map
                                    , null //@@guid
                                    , null //@@Error_Description2
                                    , null //@@Flag_Value_Chain
                                    , null //@@GROUP_NO
                                    , 0);

                            #endregion
                        }
                        else
                        {
                            #region insert into partner_order_log
                            int res = db.Execute(qInsertPartnerOrderLog,
                                    reffNo //@@Contract_Number
                                    , null //@@Product_Code
                                    , null //@@Partner_Branch_Code
                                    , null //@@Engine_Number
                                    , null //@@Chassis_Number
                                    , null //@@Period_From
                                    , null //@@Period_To
                                    , null //@@Client_Number
                                    , null //@@Vehicle_Code
                                    , null //@@Geographical_Area
                                    , null //@@Manufacturing_Year
                                    , "1" //@@Order_Type
                                    , null //@@Policy_No
                                    , null//@@Endorsement_No
                                    , null //@@Effective_Date
                                    , null //@@Name
                                    , null //@@Error_Code
                                    , errorDesc //@@Error_Description
                                    , UserID //@@entryusr
                                    , DateTime.Now //@@entrydt
                                    , null //@@updateusr
                                    , null //@@updatedt
                                    , "TAF" //@@Partner_Id
                                    , null //@@Numeric_Error_Code_1
                                    , null //@@Numeric_Error_Code_2
                                    , null //@@Object_No
                                    , null //@@Endorsement_Map
                                    , null //@@guid
                                    , null //@@Error_Description2
                                    , null //@@Flag_Value_Chain
                                    , null //@@GROUP_NO
                                    , 0
                                    );
                            #endregion
                        }
                    }
                    else if (param == "CancelOrder")
                    {
                        #region insert into partner_order_log
                        int res = db.Execute(qInsertPartnerOrderLog,
                                data.ContractNumber == null ? "" : data.ContractNumber //@@Contract_Number
                                , data.ProductCode //@@Product_Code
                                , null //@@Partner_Branch_Code
                                , null //@@Engine_Number
                                , null //@@Chassis_Number
                                , null //@@Period_From
                                , null //@@Period_To
                                , null //@@Client_Number
                                , null //@@Vehicle_Code
                                , null //@@Geographical_Area
                                , null //@@Manufacturing_Year
                                , data.Order_Type //@@Order_Type
                                , data.PolicyNumber //@@Policy_No
                                , null//@@Endorsement_No
                                , data.Effective_Date == DateTime.MinValue ? null : data.Effective_Date.ToString("MM/dd/yyyy") //@@Effective_Date
                                , null //@@Name
                                , null //@@Error_Code
                                , data.Error_Description //@@Error_Description
                                , UserID //@@entryusr
                                , DateTime.Now //@@entrydt
                                , null //@@updateusr
                                , null //@@updatedt
                                , "TAF" //@@Partner_Id
                                , data.Error_Code1 //@@Numeric_Error_Code_1
                                , data.Error_Code2 //@@Numeric_Error_Code_2
                                , null //@@Object_No
                                , null //@@Endorsement_Map
                                , null //@@guid
                                , null //@@Error_Description2
                                , null //@@Flag_Value_Chain
                                , null //@@GROUP_NO
                                , 0
                                );
                        #endregion
                    }
                    else if (param == "groupingAR")
                    {
                        #region insert log into acc_grouping_ar
                        #region query
                        string query = @"
                                DECLARE @@no_kontrak VARCHAR(50) = @0
                                        ,@@policy_no VARCHAR(50) = @1
                                        ,@@group_no VARCHAR(50) = @2
                                        ,@@approval_date VARCHAR(50) = @3
                                        ,@@type_insu VARCHAR(50) = @4
                                        ,@@endorsement_no VARCHAR(50) = @5
                                        ,@@ar_status VARCHAR(50) = @6
                                        ,@@send_status VARCHAR(50) = @7
                                        ,@@send_date VARCHAR(50) = @8
                                        ,@@error_code VARCHAR(50) = @9
                                        ,@@error_description VARCHAR(250) = @10
                                        ,@@reff_no VARCHAR(50) = @11
                                        ,@@file_name VARCHAR(50) = @12
                                        ,@@create_by VARCHAR(50) = @13
                                        ,@@create_date DATETIME = @14

                                INSERT INTO [dbo].[acc_grouping_ar]
                                           ([no_kontrak]
                                           ,[policy_no]
                                           ,[group_no]
                                           ,[approval_date]
                                           ,[type_insu]
                                           ,[endorsement_no]
                                           ,[ar_status]
                                           ,[send_status]
                                           ,[send_date]
                                           ,[error_code]
                                           ,[error_description]
                                           ,[reff_no]
                                           ,[file_name]
                                           ,[create_by]
                                           ,[create_date])
                                     VALUES
                                           (@@no_kontrak
                                           ,@@policy_no
                                           ,@@group_no
                                           ,@@approval_date
                                           ,@@type_insu
                                           ,@@endorsement_no
                                           ,@@ar_status
                                           ,@@send_status
                                           ,@@send_date
                                           ,@@error_code
                                           ,@@error_description
                                           ,@@reff_no
                                           ,@@file_name
                                           ,@@create_by
                                           ,@@create_date)";
                        #endregion

                        foreach (var item in data)
                        {
                            db.Execute(query, item.NO_KONTRAK, item.POLICY_NO, item.GROUP_NO, item.APPROVAL_DATE.ToString("dd/MM/yyyy"), item.TYPE_INSU, "0", "1", "0", "", item.ERROR_CODE, item.ERROR_DESCRIPTION, reffNo, "",
                                UserID, DateTime.Now);
                        }
                        #endregion
                    }
                    else if (param == "nullData")
                    {
                        #region insert into partner_order_log
                        int res = db.Execute(qInsertPartnerOrderLog,
                                data.ContractNumber == null ? "" : data.ContractNumber //@@Contract_Number
                                , data.ProductCode //@@Product_Code
                                , null //@@Partner_Branch_Code
                                , null //@@Engine_Number
                                , null //@@Chassis_Number
                                , null //@@Period_From
                                , null //@@Period_To
                                , null //@@Client_Number
                                , null //@@Vehicle_Code
                                , null //@@Geographical_Area
                                , null //@@Manufacturing_Year
                                , data.Order_Type //@@Order_Type
                                , data.PolicyNumber //@@Policy_No
                                , null//@@Endorsement_No
                                , data.Effective_Date == DateTime.MinValue ? null : data.Effective_Date.ToString("MM/dd/yyyy") //@@Effective_Date
                                , null //@@Name
                                , null //@@Error_Code
                                , "error get data from Maria DB" //@@Error_Description
                                , UserID //@@entryusr
                                , DateTime.Now //@@entrydt
                                , null //@@updateusr
                                , null //@@updatedt
                                , "TAF" //@@Partner_Id
                                , data.Error_Code1 //@@Numeric_Error_Code_1
                                , data.Error_Code2 //@@Numeric_Error_Code_2
                                , null //@@Object_No
                                , null //@@Endorsement_Map
                                , null //@@guid
                                , null //@@Error_Description2
                                , null //@@Flag_Value_Chain
                                , null //@@GROUP_NO
                                , 0
                                );
                        #endregion
                    }
                }
            }
            catch (Exception ex)
            {
                result.isSuccess = false;
                result.message = ex.Message;
                _log.Error(MethodBase.GetCurrentMethod().Name + "Error : " + ex.Message);
            }

            return result;
        }
        #endregion
        public class APIParam
        {
            public string RefferenceNo { get; set; }
            public string B2BToken { get; set; }
        }

    }
}